package cn.rocket.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

public class Cla_Element implements java.io.Serializable {
	private Integer claid;
	private String  claname;
	private List<Element> ele;
	public Integer getClaid() {
		return claid;
	}
	public void setClaid(Integer claid) {
		this.claid = claid;
	}
	public String getClaname() {
		return claname;
	}
	public void setClaname(String claname) {
		this.claname = claname;
	}
	public List<Element> getEle() {
		return ele;
	}
	public void setEle(List<Element> ele) {
		this.ele = ele;
	}
	

}
