package cn.rocket.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

public class Site {
	private int siteid;
	private String code;
	private String sitename;
	private Region region;
	private Classification classification;
	private String address;
	private String phone;
	private double x;
	private double y;
	private double higher;
	private List<Element> elements;
	private String intro;
	
}
