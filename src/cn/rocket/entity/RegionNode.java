package cn.rocket.entity;

import java.util.List;

public class RegionNode {

	private int id;
	private String name;
	private List<RegionNode> childnode;
	private List<Site> sites;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public List<RegionNode> getChildnode() {
		return childnode;
	}
	public void setChildnode(List<RegionNode> childnode) {
		this.childnode = childnode;
	}
	public RegionNode(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	
}
