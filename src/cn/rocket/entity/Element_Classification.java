package cn.rocket.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "element_Classification", catalog = "environment")
public class Element_Classification implements java.io.Serializable {
	private Integer ecid;
	private Integer claid;
	private Integer eid;
	@Column(name = "claid", nullable = false, length = 11)
	public Integer getClaid() {
		return claid;
	}
	public void setClaid(Integer claid) {
		this.claid = claid;
	}
	@Column(name = "eid", nullable = false, length = 11)
	public Integer getEid() {
		return eid;
	}
	public void setEid(Integer eid) {
		this.eid = eid;
	}
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ecid", unique = true, nullable = false)
	public Integer getEcid() {
		return ecid;
	}
	public void setEcid(Integer ecid) {
		this.ecid = ecid;
	}
	public Element_Classification() {
	}


	public Element_Classification(int claid,int eid) {
		// TODO Auto-generated constructor stub
		this.claid = claid;
		this.eid = eid;
	}
	public Element_Classification(int ecid,int claid,int eid) {
		// TODO Auto-generated constructor stub
		this.ecid = ecid;
		this.claid = claid;
		this.eid = eid;
	}

}
