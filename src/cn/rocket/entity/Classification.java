package cn.rocket.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "classification", catalog = "environment")
public class Classification implements java.io.Serializable {
	private Integer claid;
	private String  claname;
	@Column(name = "claname", nullable = false, length = 45)
	public String getClaname() {
		return claname;
	}
	public void setClaname(String claname) {
		this.claname = claname;
	}
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "claid", unique = true, nullable = false)
	public Integer getClaid() {
		return claid;
	}
	public void setClaid(Integer claid) {
		this.claid = claid;
	}
	public Classification() {
	}


	public Classification(String claname) {
		// TODO Auto-generated constructor stub
		this.claname = claname;
	}

}
