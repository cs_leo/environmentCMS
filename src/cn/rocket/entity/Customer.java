package cn.rocket.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "customer", catalog = "environment")
public class Customer implements java.io.Serializable {
	private Integer cid;
	private String  cname;
	@Column(name = "cname", nullable = false, length = 45)
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "cid", unique = true, nullable = false)
	public Integer getCid() {
		return cid;
	}
	public void setCid(Integer cid) {
		this.cid = cid;
	}
	public Customer() {
	}


	public Customer(String cname) {
		// TODO Auto-generated constructor stub
		this.cname = cname;
	}
	public Customer(int cid){
		this.cid = cid;
	}
}
