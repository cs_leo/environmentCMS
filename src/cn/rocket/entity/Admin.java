package cn.rocket.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "admin", catalog = "environment")
public class Admin {
    private int aid;
    private String aname;
    private String apassword;
    private String role;
    private Region region;
    private Customer customer;
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "aid", unique = true, nullable = false)
	public int getAid() {
		return aid;
	}
	public void setAid(int aid) {
		this.aid = aid;
	}
	public String getAname() {
		return aname;
	}
	public void setAname(String aname) {
		this.aname = aname;
	}
	public String getApassword() {
		return apassword;
	}
	public void setApassword(String apassword) {
		this.apassword = apassword;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	//配1对1注解
	@OneToOne
	@JoinColumn(name = "rid")
	public Region getRegion() {
		return region;
	}
	public void setRegion(Region region) {
		this.region = region;
	}
	//配1对1注解
	@OneToOne
	@JoinColumn(name = "cid")
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public Admin(int aid){
	    this.aid = aid;
	}
	public Admin(String aname,String apassword,String role,Region region,Customer customer){
		this.aname = aname;
		this.apassword = apassword;
		this.role = role;
		this.region = region;
		this.customer = customer;
	}
	public Admin(int aid,String aname,String apassword,String role,Region region,Customer customer){
		this(aname, apassword, role, region, customer);
		this.aid = aid;
	}
	public Admin(){}
}
