package cn.rocket.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "element", catalog = "environment")
public class Element implements java.io.Serializable {
	private Integer eid;
	private String  ename;
	private String  edatatype;
	private String  eunittype;
	public Element() {
	}
	public Element(String ename,String edatatype,String eunittype) {
		this.ename=ename;
		this.edatatype=edatatype;
		this.eunittype=eunittype;
	}
	public Element(int eid,String ename,String edatatype,String eunittype) {
		this.eid=eid;
		this.ename=ename;
		this.edatatype=edatatype;
		this.eunittype=eunittype;
	}
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "eid", unique = true, nullable = false)
	public Integer getEid() {
		return eid;
	}
	public void setEid(Integer eid) {
		this.eid = eid;
	}
	
	@Column(name = "ename", nullable = false, length = 45)
	public String getEname() {
		return ename;
	}
	public void setEname(String ename) {
		this.ename = ename;
	}
	
	
	@Column(name = "edatatype", nullable = false, length = 45)
	public String getEdatatype() {
		return edatatype;
	}
	public void setEdatatype(String edatatype) {
		this.edatatype = edatatype;
	}
	@Column(name = "eunittype", nullable = false, length = 45)
	public String getEunittype() {
		return eunittype;
	}
	public void setEunittype(String eunittype) {
		this.eunittype = eunittype;
	}




}
