package cn.rocket.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "region", catalog = "environment")
public class Region {
	private int rid;
	private String rname;
	private int rhigherLevel;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "rid", unique = true, nullable = false)
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	public String getRname() {
		return rname;
	}
	public void setRname(String rname) {
		this.rname = rname;
	}
	public int getRhigherLevel() {
		return rhigherLevel;
	}
	public void setRhigherLevel(int rhigherLevel) {
		this.rhigherLevel = rhigherLevel;
	}
	public Region(int rid, String rname, int rhigherLevel) {
		super();
		this.rid = rid;
		this.rname = rname;
		this.rhigherLevel = rhigherLevel;
	}
	public Region() {
	}
	public Region(int rid) {
		this.rid=rid;
	}
	public Region(String rname, int rhigherLevel) {
		super();
		this.rname = rname;
		this.rhigherLevel = rhigherLevel;
	}
	
}
