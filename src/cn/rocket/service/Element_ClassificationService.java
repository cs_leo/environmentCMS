package cn.rocket.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.rocket.dao.ClassificationDao;
import cn.rocket.dao.ElementDao;
import cn.rocket.dao.Element_ClassificationDao;
import cn.rocket.entity.Cla_Element;
import cn.rocket.entity.Classification;
import cn.rocket.entity.Element;
import cn.rocket.entity.Element_Classification;

import com.alibaba.fastjson.JSON;

@Service("element_ClassificationService")
public class Element_ClassificationService {
	@Autowired
	private Element_ClassificationDao e_ClaDao;
	@Autowired
	private ElementDao eDao;
	@Autowired
	private ClassificationDao claDao;

	public String addE_Cla( String claname, String[] eidlist) {

		List<Classification> clalist = claDao.findAll();
		for (int i = 0; i < clalist.size(); i++) {
			if (claname.equals(clalist.get(i).getClaname())) {
				return "0";
			}
		}
		Classification cla = new Classification();
		cla.setClaname(claname);
		claDao.add(cla);
		clalist=claDao.findByProperty("model.claname='"+claname+"'");
		int clid=clalist.get(0).getClaid();
		int[] eid = new int[eidlist.length];
		for (int i = 0; i < eidlist.length; i++) {
			eid[i] = Integer.parseInt(eidlist[i]);
		}

		for (int k = 0; k < eid.length; k++) {
			Element_Classification ele_cla = new Element_Classification();
			ele_cla.setClaid(clid);
			ele_cla.setEid(eid[k]);
			e_ClaDao.add(ele_cla);
		}

		return "1";
	}

	public String getE_Cla() {
		System.out.println(1123);List<Cla_Element> cla_elelist = new ArrayList<Cla_Element>();
		List<Classification> clalist = claDao.findAll();
		for (int i = 0; i < clalist.size(); i++) {
			Cla_Element cla_element = new Cla_Element();
			int claid = clalist.get(i).getClaid();
			cla_element.setClaid(claid);
			cla_element.setClaname(clalist.get(i).getClaname());
			List<Element_Classification> ele_clalist = e_ClaDao
					.findByProperty("model.claid=" + claid);
			List<Element> elelist = new ArrayList<Element>();
			for (int k = 0; k < ele_clalist.size(); k++) {
				Element ele = eDao.findById(ele_clalist.get(k).getEid());
				elelist.add(ele);
			}
			cla_element.setEle(elelist);
			cla_elelist.add(cla_element);
		}
		System.out.println(cla_elelist);
		return JSON.toJSONString(cla_elelist);
	}

	public String updateE_Cla(String claid, String claname, String[] eidlist) {
		int clid = Integer.valueOf(claid);
		Classification cla = claDao.findById(clid);
		cla.setClaname(claname);
		List<Element_Classification> ele_clalist = e_ClaDao
				.findByProperty("model.claid=" + clid);
		for (int i = 0; i < ele_clalist.size(); i++) {
			try {
				e_ClaDao.delete(ele_clalist.get(i));
			} catch (Exception e) {
				// TODO 自动生成的 catch 块

				e.printStackTrace();
			}
		}
		int[] eid = new int[eidlist.length];
		for (int i = 0; i < eidlist.length; i++) {
			eid[i] = Integer.parseInt(eidlist[i]);
		}
		for (int k = 0; k < eid.length; k++) {
			Element_Classification ele_cla = new Element_Classification();
			ele_cla.setClaid(clid);
			ele_cla.setEcid(eid[k]);
			e_ClaDao.add(ele_cla);
		}

		return "1";
	}

	public String deteleE_Cla(String claid) {
		int clid = Integer.valueOf(claid);
		List<Element_Classification> ele_clalist = e_ClaDao
				.findByProperty("model.claid=" + clid);
		for (int i = 0; i < ele_clalist.size(); i++) {
			try {
				e_ClaDao.delete(ele_clalist.get(i));
			} catch (Exception e) {
				// TODO 自动生成的 catch 块

				e.printStackTrace();
			}
		}
		Classification cla = claDao.findById(clid);
		try {
			claDao.delete(cla);
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		return "1";

	}

}
