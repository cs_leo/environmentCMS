package cn.rocket.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.rocket.dao.AdminDao;
import cn.rocket.dao.CustomerDao;
import cn.rocket.entity.Customer;

import com.alibaba.fastjson.JSON;

@Service("customerService ")
public class CustomerService {
	@Resource
	CustomerDao customerDao;
    public String showCustomer(){
    	return JSON.toJSONString(customerDao.findAll());
    }
    
    /**
	 * 
	 * @param username
	 * @param password
	 * @return 0 用户名错误 1 密码错误 10 登陆成功
	 */
	public String addCus(String cname) {
		Customer entity = new Customer(cname);
		try{
			customerDao.add(entity);
			return "1";
		}catch(Exception e){
			//customerDao.
			return "0";
		}
		
	}
	public String getCus() {
		return JSON.toJSONString(customerDao.findAll());
	}
	public String updateCus(String cid,String cname) {
		try{
			Customer cus=customerDao.findById(Integer.valueOf(cid));
			cus.setCname(cname);
			customerDao.update(cus);
			return "1";
		}catch(Exception e){
			//customerDao.
			return "0";
		}
	}
	public String deteleCus(String cid) {
		try{
			Customer cus=customerDao.findById(Integer.valueOf(cid));
		
			customerDao.delete(cus);
			return "1";
		}catch(Exception e){
			//customerDao.
			return "0";
		}
	}


}
