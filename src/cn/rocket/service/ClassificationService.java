package cn.rocket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.rocket.dao.ClassificationDao;
import cn.rocket.entity.Classification;

import com.alibaba.fastjson.JSON;

@Service("classificationService")
public class ClassificationService {
	@Autowired
	private ClassificationDao classificationDao;

	
	/**
	 * 
	 * @param username
	 * @param password
	 * @return 0 用户名错误 1 密码错误 10 登陆成功
	 */
	public String addCla(String claname) {
		Classification entity = new Classification(claname);
		try{
			classificationDao.add(entity);
			return "1";
		}catch(Exception e){
			//customerDao.
			return "0";
		}
		
	}
	public String getClaName(int claid) {
		Classification entity=classificationDao.findById(claid);
		String result=entity.getClaname();
		return result;
	}
	public String updateCla(String claid,String claname) {
		try{
			Classification entity=classificationDao.findById(Integer.valueOf(claid));
			entity.setClaname(claname);
			classificationDao.update(entity);
			return "1";
		}catch(Exception e){
			//customerDao.
			return "0";
		}
	}
	public String deteleCla(String claid) {
		try{
			Classification entity=classificationDao.findById(Integer.valueOf(claid));
		
			classificationDao.delete(entity);
			return "1";
		}catch(Exception e){
			//customerDao.
			return "0";
		}
	}

}
