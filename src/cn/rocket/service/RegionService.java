package cn.rocket.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.rocket.dao.RegionDao;
import cn.rocket.entity.Admin;
import cn.rocket.entity.Region;
import cn.rocket.entity.RegionNode;

import com.alibaba.fastjson.JSON;



@Service("regionService")
public class RegionService {
	@Resource
	RegionDao regionDao;
	
	public String showRegion() {
		// TODO Auto-generated method stub
		return JSON.toJSONString(regionDao.findAll());
	}
	public String addRegion(String quyumingcheng, String shangjiquyu) {
		// TODO Auto-generated method stub
		int higherLevel = Integer.parseInt(shangjiquyu);
		Region entity = new Region(quyumingcheng,higherLevel);
		try{
			regionDao.add(entity);
			return "1";
		}catch(Exception e){
			return "0";
		}
	}
    public String deleteRegion(String rid){
    	int delRid = Integer.parseInt(rid);
    	Region entity = new Region(delRid);
    	try{
    		regionDao.delete(entity);
    		return "1";
    	}catch(Exception e){
    		return "0";
    	}
    }
    public String findRegionById(String rid){
    	List<Region> regions = regionDao.findByProperty("model.rid="+rid);
    	return JSON.toJSONString(regions);
    }
    public String updateRegion(String rid,String rname,String rhigherLevel){
    	
    	System.out.println(rid+"--"+rname+"--"+rhigherLevel);
    	
    	int upRid = Integer.parseInt(rid);
    	int upRhigherLevel = Integer.parseInt(rhigherLevel);
    	Region entity = new Region(upRid,rname,upRhigherLevel);
    	try{
    		regionDao.update(entity);
            return "1";    		
    	}catch(Exception e){
    		return "0";
    	}
    }
    
    public String regionTree(){
    	List<Region> rootList = regionDao.findByProperty("model.rhigherLevel="+0);	//找到所有根节点
    	List<RegionNode> list = new ArrayList<RegionNode>();				//返回值
    	for (int i = 0; i<rootList.size(); i++){							//遍历根节点
    		RegionNode root = new RegionNode(rootList.get(i).getRid(),rootList.get(i).getRname());	//
    		//System.out.println(root.getName()+"   "+root.getId());
    		List<RegionNode> rn = new ArrayList<RegionNode>();				//初始化孩子节点list
    		root.setChildnode(rn);
    		List<Region> childList = regionDao.findByProperty("model.rhigherLevel="+rootList.get(i).getRid());//找孩子节点
    		//System.out.println(childList.get(0).getRid());
    		if (childList.size() != 0){
        		childRegionTree(childList,root);
        	}
    		list.add(root);
    	}
    	return JSON.toJSONString(list);
    }
    
    public void childRegionTree(List<Region> rootList,RegionNode root){
    	for (int i = 0; i<rootList.size(); i++){
    		RegionNode child = new RegionNode(rootList.get(i).getRid(),rootList.get(i).getRname());
    		List<RegionNode> rn = new ArrayList<RegionNode>();				//初始化孩子节点list
    		child.setChildnode(rn);
    		List<Region> childList = regionDao.findByProperty("model.rhigherLevel="+rootList.get(i).getRid());
    		for(int j = 0; j<childList.size(); j++){
    			if (childList.size() != 0){
        			childRegionTree(childList,child);
        		}
    		}
    		root.getChildnode().add(child);
    	}
    }
}
