package cn.rocket.service;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;

import cn.rocket.dao.ElementDao;
import cn.rocket.entity.Element;

@Service("elementService")
public class ElementService {
	@Autowired
	private ElementDao elementDao;

	
	/**
	 * 
	 * @param username
	 * @param password
	 * @return 0 用户名错误 1 密码错误 10 登陆成功
	 */															
	public String addEle(String ename,String edatatype, String eunittype) {
		Element entity=new Element(ename,edatatype,eunittype) ;
		try{
			elementDao.add(entity);
			return "1";
		}catch(Exception e){
			//elementDao.
			return "0";
		}
		
	}
	public String getEle() {
		String data=JSON.toJSONString(elementDao.findAll());
		return data;
	}
	public String getEleName(int eid) {
		String data=elementDao.findById(Integer.valueOf(eid)).getEname();
		return data;
	}
	public String updateEle(String eid,String ename) {
		try{
			Element ele=elementDao.findById(Integer.valueOf(eid));
			ele.setEname(ename);
			elementDao.update(ele);
			return "1";
		}catch(Exception e){
			//elementDao.
			return "0";
		}
	}
	public String deteleEle(String eid) {
		try{
			Element ele=elementDao.findById(Integer.valueOf(eid));
		
			elementDao.delete(ele);
			return "1";
		}catch(Exception e){
			//elementDao.
			return "0";
		}
	}

}
