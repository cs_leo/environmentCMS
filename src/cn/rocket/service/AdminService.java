package cn.rocket.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;

import cn.rocket.dao.AdminDao;
import cn.rocket.entity.Admin;
import cn.rocket.entity.Customer;
import cn.rocket.entity.Region;


@Service("adminService ")
public class AdminService {
	@Resource
	AdminDao adminDao;
    public String showAdmin(){
    	return JSON.toJSONString(adminDao.findAll());
    }
    public String addAdmin(String aname,String apassword,String role,String rid,String cid){
    	int myRid = Integer.parseInt(rid);
    	Region region = new Region(myRid);
    	int myCid = Integer.parseInt(cid);
    	Customer customer = new Customer(myCid);
    	Admin entity = new Admin(aname, apassword, role, region,customer);
    	try{
    		adminDao.add(entity);
            return "1";    		
    	}catch(Exception e){
    		return "0";
    	}
    }
    public String deleteAdmin(String aid){
    	int myAid = Integer.parseInt(aid);
    	Admin entity = new Admin(myAid);
    	try {
			adminDao.delete(entity);
			return "1";
		} catch (Exception e) {
			// TODO: handle exception
			return "0";
		}
    }
    public String updateAdmin(String aid,String aname,String apassword,String role,String rid,String cid){
    	int myAid = Integer.parseInt(aid);
    	int myCid = Integer.parseInt(cid);
    	Customer customer = new Customer(myCid);
    	int myRid = Integer.parseInt(rid);
    	Region region = new Region(myRid);
    	Admin entity = new Admin(myAid, aname, apassword, role, region, customer);
    	try {
			adminDao.update(entity);
			return "1";
		} catch (Exception e) {
			// TODO: handle exception
			return "0";
		}
    }
}
