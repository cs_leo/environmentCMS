package cn.rocket.model;

public class HistoriesModel {
	public String[][] data;

	public String[][] getData() {
		return data;
	}

	public void setData(String[][] data) {
		this.data = data;
	}
}
