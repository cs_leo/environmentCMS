package cn.rocket.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;

public class BaseAction {
	protected SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

	public void printJSON(HttpServletResponse response, Object obj) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Content-type", "application/json");
		try {
			PrintWriter out = response.getWriter();
			out.print(JSON.toJSONString(obj));
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void out(HttpServletResponse response, String content) {
		try {
			response.setHeader("Content-type", "application/json");
			PrintWriter out = response.getWriter();
			out.print(content);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
