package cn.rocket.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSON;

import cn.rocket.entity.Region;
import cn.rocket.service.RegionService;

@Controller("regionAction")
@RequestMapping("/region.action")
public class RegionAction extends BaseAction{
	@Resource
	RegionService regionService;
	@RequestMapping(method = RequestMethod.POST,params="showRegion")
	public void showRegion(HttpServletResponse response){
		String result = regionService.showRegion();
		System.out.println(result);
		out(response,result);
	}
	@RequestMapping(method = RequestMethod.POST,params="addRegion")
	public void addRegion(String quyumingcheng,String shangjiquyu,HttpServletResponse response){
		String result = regionService.addRegion(quyumingcheng,shangjiquyu);
		System.out.println(result);
		out(response,result);
	}
	@RequestMapping(method = RequestMethod.POST,params = "deleteRegion")
	public void deleteRegion(String rid,HttpServletResponse response){
		String result = regionService.deleteRegion(rid);
		System.out.println(result);
		out(response,result);
	}
	@RequestMapping(method = RequestMethod.POST,params = "updateRegion")
	public void updateRegion(String rid,String rname,String rhigherLevel,HttpServletResponse response){
		String result = regionService.updateRegion(rid,rname,rhigherLevel);
		System.out.println(result);
		out(response,result);
	}
	@RequestMapping(method = RequestMethod.POST,params = "findRegionById")
	public void findRegionById(String rid,HttpServletResponse response){
		String result = regionService.findRegionById(rid);
		System.out.println(result);
		out(response,result);
	}
	
	@RequestMapping(method = RequestMethod.POST,params = "regionTree")
	public void regionTree(HttpServletResponse response){
		String result = regionService.regionTree();
		System.out.println(result);
		out(response,result);
	}
}
