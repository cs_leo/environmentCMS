package cn.rocket.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.rocket.service.CustomerService;
import cn.rocket.service.ElementService;

@Controller("elementAction")
@RequestMapping("/element.action")
public class ElementAction extends BaseAction{
	@Resource
	private ElementService elementService;
	@RequestMapping(method = RequestMethod.POST, params = "addEle")
	public void add_element(String ename,String edatatype,String eunittype,HttpServletResponse response, HttpServletRequest request) throws IOException{
		String result=elementService.addEle(ename,edatatype,eunittype);
		out(response,result);
	}
	@RequestMapping(method = RequestMethod.POST, params = "showEle")
	public void show_element(HttpServletResponse response){
		String result = elementService.getEle();
		System.out.println(result);
		out(response,result);
	}
	@RequestMapping(method = RequestMethod.POST, params = "updateEle")
	public void update_element(String eid,String ename,HttpServletResponse response){
		String result = elementService.updateEle(eid, ename);
		out(response,result);
	}
	@RequestMapping(method = RequestMethod.POST, params = "deteleEle")
	public void detele_element(String eid,HttpServletResponse response){
		String result = elementService.deteleEle(eid);
		out(response,result);
	}
}
