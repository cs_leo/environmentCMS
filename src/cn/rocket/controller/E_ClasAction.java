package cn.rocket.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSON;

import cn.rocket.entity.Element_Classification;
import cn.rocket.service.ClassificationService;
import cn.rocket.service.ElementService;
import cn.rocket.service.Element_ClassificationService;

@Controller("e_ClaAction")
@RequestMapping("/e_Cla.action")
public class E_ClasAction extends BaseAction{
	@Resource
	private ClassificationService claService;
	@Resource
	private Element_ClassificationService e_claService;
	@Resource
	private ElementService eService;
	@RequestMapping(method = RequestMethod.POST, params = "addE_Cla")
	public void add_E_Cla(String claname,String [] elelist,HttpServletResponse response, HttpServletRequest request) throws IOException{
		String result=e_claService.addE_Cla( claname, elelist);
		out(response,result);
	}

	@RequestMapping(method = RequestMethod.POST, params = "showE_Cla")
	public void show_E_Cla(HttpServletResponse response){
		String result = e_claService.getE_Cla();
		out(response,result);
	}
	@RequestMapping(method = RequestMethod.POST, params = "updateE_Cla")
	public void update_customer(String cid,String cname,String [] eidlist,HttpServletResponse response){
		String result = e_claService.updateE_Cla(cid, cname,eidlist);
		out(response,result);
	}
	@RequestMapping(method = RequestMethod.POST, params = "deteleE_Cla")
	public void detele_customer(String cid,HttpServletResponse response){
		String result = e_claService.deteleE_Cla(cid);
		out(response,result);
	}
}
