package cn.rocket.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.rocket.permission.Permission;
import cn.rocket.service.AdminService;

@Controller("adminAction")
@RequestMapping("/admin.action")
public class AdminAction extends BaseAction {
    @Resource
    AdminService adminService;
    @RequestMapping(method = RequestMethod.POST,params="showAdmin")
    public void showAdmin(HttpServletResponse response){
    	String result = adminService.showAdmin();
    	System.out.println(result);
    	out(response,result);
    }
    @RequestMapping(method = RequestMethod.POST,params = "addAdmin")
    public void addAdmin(String aname,String apassword,String role,String rid,String cid,HttpServletResponse response){
    	String result = adminService.addAdmin(aname, apassword, role, rid, cid);
    	System.out.println(result);
    	out(response,result);
    }
    @RequestMapping(method = RequestMethod.POST,params = "deleteAdmin")
    public void deleteAdmin(String aid,HttpServletResponse response){
    	String result = adminService.deleteAdmin(aid);
    	System.out.println(result);
    	out(response,result);
    }
    @RequestMapping(method = RequestMethod.POST,params = "updateAdmin")
    public void updateAdmin(String aid,String aname,String apassword,String role,String rid,String cid,HttpServletResponse response){
    	String result = adminService.updateAdmin(aid,aname,apassword,role,rid,cid);
    	System.out.println(result);
    	out(response, result);
    }
}
