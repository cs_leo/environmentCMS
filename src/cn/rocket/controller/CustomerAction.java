package cn.rocket.controller;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.rocket.service.AdminService;
import cn.rocket.service.CustomerService;

@Controller("customerAction")
@RequestMapping("/customer.action")
public class CustomerAction extends BaseAction{
	@Resource
    CustomerService customerService;
    @RequestMapping(method = RequestMethod.POST,params="showCustomer")
    public void showCustomer(HttpServletResponse response){
    	String result = customerService.showCustomer();
    	System.out.println(result);
    	out(response,result);
    }
    
    @RequestMapping(method = RequestMethod.POST, params = "addCus")
	public void add_customer(String cname,HttpServletResponse response, HttpServletRequest request) throws IOException{
		String result=customerService.addCus(cname);
		out(response,result);
	}
	@RequestMapping(method = RequestMethod.POST, params = "showCus")
	public void show_customer(HttpServletResponse response){
		String result = customerService.getCus();
		out(response,result);
	}
	@RequestMapping(method = RequestMethod.POST, params = "updateCus")
	public void update_customer(String cid,String cname,HttpServletResponse response){
		String result = customerService.updateCus(cid, cname);
		out(response,result);
	}
	@RequestMapping(method = RequestMethod.POST, params = "deteleCus")
	public void detele_customer(String cid,HttpServletResponse response){
		String result = customerService.deteleCus(cid);
		out(response,result);
	}
}
