package cn.rocket.dao;

import java.io.Serializable;
import java.util.List;

public interface IBaseDao<T>{
	public void add(T entity);
	public void delete(T entity) throws Exception;
	public void update(T entity);
	public T findById(java.io.Serializable id);
	public List<T> findAll();
	public List<T> findByProperty(Serializable param);
	public List<T> findBySearch(String str);
	public List excuteQuery(String sql);
	public int updateSQL(String sql);
	public void attachDirty(T entity);
}