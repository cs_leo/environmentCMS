package cn.rocket.dao.impl;

import org.springframework.stereotype.Repository;

import cn.rocket.dao.RegionDao;
import cn.rocket.entity.Region;

@Repository("regionDao")
public class RegionDaoImpl extends BaseDaoImpl<Region> implements RegionDao{
	public RegionDaoImpl() {
		super(Region.class);
	}
}
