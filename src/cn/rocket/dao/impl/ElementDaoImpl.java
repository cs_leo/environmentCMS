package cn.rocket.dao.impl;

import org.springframework.stereotype.Repository;

import cn.rocket.dao.ElementDao;
import cn.rocket.entity.Element;


@Repository("elementDao")
public class ElementDaoImpl extends BaseDaoImpl<Element> implements ElementDao{

	public ElementDaoImpl() {
		super(Element.class);
		// TODO Auto-generated constructor stub
	}
}
