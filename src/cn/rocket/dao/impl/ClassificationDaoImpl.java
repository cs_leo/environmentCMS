package cn.rocket.dao.impl;

import org.springframework.stereotype.Repository;

import cn.rocket.dao.ClassificationDao;
import cn.rocket.entity.Classification;


@Repository("classificationDao")
public class ClassificationDaoImpl extends BaseDaoImpl<Classification> implements ClassificationDao{

	public ClassificationDaoImpl() {
		super(Classification.class);
		// TODO Auto-generated constructor stub
	}
}
