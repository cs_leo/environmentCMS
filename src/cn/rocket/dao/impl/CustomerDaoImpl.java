package cn.rocket.dao.impl;

import org.springframework.stereotype.Repository;

import cn.rocket.dao.CustomerDao;
import cn.rocket.entity.Customer;

@Repository("customerDao")
public class CustomerDaoImpl extends BaseDaoImpl<Customer> implements CustomerDao{
		public CustomerDaoImpl() {
			super(Customer.class);
		}
}
