package cn.rocket.dao.impl;

import org.springframework.stereotype.Repository;

import cn.rocket.dao.Element_ClassificationDao;
import cn.rocket.entity.Element_Classification;


@Repository("element_ClassificationDao")
public class Element_ClassificationDaoImpl extends BaseDaoImpl<Element_Classification> implements Element_ClassificationDao{

	public Element_ClassificationDaoImpl() {
		super(Element_Classification.class);
		// TODO Auto-generated constructor stub
	}
}
