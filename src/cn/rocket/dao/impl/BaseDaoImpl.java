package cn.rocket.dao.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import cn.rocket.dao.IBaseDao;
import cn.rocket.dao.IBaseHibernateDAO;
import cn.rocket.util.Config;

public class BaseDaoImpl<T> implements IBaseDao<T>, IBaseHibernateDAO {
	private Class<T> cls;
	@Resource
	protected SessionFactory sessionFactory;

	@Resource
	protected JdbcTemplate jdbcTemplate;

	public BaseDaoImpl(Class<T> cls) {
		this.cls = cls;
	}

	public List excuteQuery(String sql) {
		List<Map<String, Object>> mapList = (List<Map<String, Object>>) jdbcTemplate.queryForList(sql);
		return mapList;
	}
	
	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	public int updateSQL(String sql){
		int count = jdbcTemplate.update(sql);
		return count;
	}
	
	public void add(T entity) {
		try {
			getSession().save(entity);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void attachDirty(T entity) {
		try {
			getSession().saveOrUpdate(entity);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void delete(T entity) {
		try {
			getSession().delete(entity);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Integer deleteByIds(String str) {
		int result = 0;
		try {
			result = getSession().createQuery("delete from " + cls.getName() + " data where data.id " + str).executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;

	}

	public void update(T entity) {
		try {
			getSession().update(entity);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	public T findById(Serializable id) {
		T t = null;
		try {
			t = (T) getSession().get(cls, id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return t;

	}

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		List<T> list = null;
		try {
			Query query = getSession().createQuery("from " + cls.getName());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<T> findByProperty(Serializable param) {
		String queryString = "from " + cls.getName() + " as model where " + param;
		System.out.println(queryString);
		Query queryObject = getSession().createQuery(queryString);
		return queryObject.list();
	}

	public Integer findCount() {
		int temp = -1;
		try {
			Query query = getSession().createQuery("from " + cls.getName());
			temp = query.list().size();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return temp;
	}

	@SuppressWarnings("unchecked")
	public List<T> findAllByPage(int page) {
		List<T> list = null;
		try {
			Query query = getSession().createQuery("from " + cls.getName());
			if (page == -1) {

			} else {
				query.setFirstResult(page * Config.items);
				query.setMaxResults(Config.items);
			}
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<T> findBySearch(String str) {
		List<T> list = null;
		try {
			Query query = getSession().createQuery("from " + cls.getName() + " where " + str);
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
}
