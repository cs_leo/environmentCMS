package cn.rocket.dao.impl;

import org.springframework.stereotype.Repository;

import cn.rocket.dao.AdminDao;
import cn.rocket.entity.Admin;

@Repository("adminDao")
public class AdminDaoImpl extends BaseDaoImpl<Admin> implements AdminDao{
	public AdminDaoImpl() {
		super(Admin.class);
	}
}
