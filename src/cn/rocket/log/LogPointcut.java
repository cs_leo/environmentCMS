package cn.rocket.log;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class LogPointcut {

	@Pointcut("execution(* cn.rocket.service..*.*(..))")
	public void inServiceLayer() {
	}
	
	@Pointcut("execution(* cn.rocket.dao..*.*(..))")
	public void inDaoLayer(){
	}
	
	@Pointcut("execution(* cn.rocket.controller..*.*(..))")
	public void inControllerLayer(){}

}
